
var bolnisniceUrl = 'https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json';
//Razdalja v km znotraj katere so bolnisnice obarvane z zeleno
var najblizjeBolnisnice = 3;

var lat = 46.123456;
var lng = 11.212346;
var mapa;
var button;
var currentMarker;

var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
        'marker-icon-2x-blue.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
        'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});


function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(getPosition);
    } else {
        alert('geolocation not supported');
    }
}

function getPosition(position) {

    console.log(position);

    lat = position.coords.latitude;
    lng = position.coords.longitude;

    setMap(lat, lng);
}

function initMapa(){
    var mapOptions = {
        center: [lat, lng],
        zoom: 15
        // maxZoom: 3
    };

    // Ustvarimo objekt mapa
    mapa = new L.map('mapa_id', mapOptions);
    // Ustvarimo prikazni sloj mape
    var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

    // Prikazni sloj dodamo na mapo
    mapa.addLayer(layer);
    mapa.on('click', onMapClick);
}

function displayBolnice(around) {

    $.get(bolnisniceUrl, function(data){

        data.features.forEach(function(bolnica){
            var latlngs = [];
            var color = 'blue';

            if(bolnica.properties.type === 'multipolygon'){
                //console.log('evo me');
                bolnica.geometry.coordinates[0].forEach(function(coordinate){
                    coordinate.reverse();
                });
                bolnica.geometry.coordinates[1].forEach(function(coordinate){
                    coordinate.reverse();
                });

                var closest = distance(bolnica.geometry.coordinates[0][0][0], bolnica.geometry.coordinates[0][0][1], around.lat, around.lng, 'K');

                if(closest <= najblizjeBolnisnice){
                    color = 'green'
                }

                latlngs = [bolnica.geometry.coordinates[0], bolnica.geometry.coordinates[1]];
                var polygon = L.polyline(latlngs, {color: color});
                polygon.bindPopup("<h5>" + bolnica.properties.name + "</h5>" + "<h6>" + bolnica.properties['addr:street'] + " " + bolnica.properties['addr:housenumber'] + '</h6>');
                polygon.addTo(mapa);
            }
            else if(bolnica.geometry.type === 'Point'){
                /* bolnica.geometry.coordinates.reverse();
                 var marker = L.marker(bolnica.geometry.coordinates, {icon: ikona}).addTo(mapa);*/
                return;
            }
            else if(bolnica.geometry.type === 'LineString'){
                bolnica.geometry.coordinates.forEach(function(coordinate){
                    coordinate.reverse();
                });

                var dist = distance(bolnica.geometry.coordinates[0][0], bolnica.geometry.coordinates[0][1], around.lat, around.lng, 'K');

                if(dist <= najblizjeBolnisnice){
                    color = 'green';
                }

                var polygon = L.polygon(bolnica.geometry.coordinates, {color: color}).addTo(mapa);
            }
            else{
                try {
                    bolnica.geometry.coordinates[0].forEach(function (coordinate) {
                        // console.log(coordinate);
                        coordinate.reverse();
                    });
                    latlngs = bolnica.geometry.coordinates[0];
                }catch(err){
                    console.log(err);
                    console.log(bolnica);
                }

                var closest = distance(latlngs[0][0], latlngs[0][1], around.lat, around.lng, 'K');

                if(closest <= najblizjeBolnisnice){
                    color = 'green'
                }
                var polygon = L.polygon(latlngs, {color: color})
                polygon.bindPopup("<h5>" + ((bolnica.properties.name === undefined)? "Ni podatkov" : bolnica.properties.name)  + "</h5>" + "<h6>" + ((bolnica.properties['addr:street'] === undefined)? "Ni podatkov" : bolnica.properties['addr:street'] + " " + bolnica.properties['addr:housenumber'])+ '</h6>');
                polygon.addTo(mapa);
            }

        })

    });

}

function setMap(lat, lng){
    mapa.panTo(new L.LatLng(lat, lng));
    button.attr('disabled', false);
}

function onMapClick(e){
    var latlng = e.latlng;
    clearMap();
    clearMarker();

    currentMarker = L.marker([latlng.lat, latlng.lng],{icon: ikona}).addTo(mapa);

    displayBolnice(latlng);
}

function clearMarker(){
    try{
        mapa.removeLayer(currentMarker);
    }catch (e) {
        console.log(e);
    }
}
function clearMap() {
    for(i in mapa._layers) {
        if(mapa._layers[i]._path != undefined) {
            try {
                mapa.removeLayer(mapa._layers[i]);
            }
            catch(e) {
                console.log("problem with " + e + mapa._layers[i]);
            }
        }
    }
}


$(function(){
    button = $('#dodajBolnisnice');
    button.attr('disabled', true);

    console.log(lat, lng);

    initMapa();

    getLocation();

    button.click(function(){
        clearMarker();
        clearMap();

        currentMarker = L.marker([lat, lng],{icon: ikona}).addTo(mapa);

        mapa.panTo(new L.LatLng(lat, lng));

        displayBolnice({lat, lng})
    });
});