var baseUrl = 'https://rest.ehrscape.com/rest/v1';

var username = "ois.seminar";
var password = "ois4fri";

var myChartDis;
var myChartSis;

var first = true;
var currentActive = 1;

var referenceData = {
    diastolic: 80,
    sistolic: 120
};


//a8ebf23f-6047-4511-bedd-ef80fd8d4c53
//81b306cf-db5a-4fb9-8552-5a0e9cf671b1 teo
//b93581e1-5e27-4cc6-a09d-d24499d67e72 klara
//6685482f-90bc-4f98-b7e6-e342eddc3267 nikola
//https://www.drugs.com/search.php?searchterm=Hypertension&a=1


var oseba1 = {
    num: 1,
    ime: 'Jaka',
    priimek: 'Horvat',
    datum_rojstva: '1970-03-21',
    vitalni_znaki: {

        datum_ura: '2019-05-05T12:40Z',
        telesna_visina: 185,
        telesna_teza: 78.00,
        telesna_temperatura: 36.50,
        sistolicni_tlak: 120,
        diastolicni_tlak: 80,
        nasicenost_kisik: 98

    }
};

var oseba2 = {
    num: 2,
    ime: 'Maja',
    priimek: 'Zupancic',
    datum_rojstva: '1990-01-03',
    vitalni_znaki: {

        datum_ura: '2019-05-05T12:44Z',
        telesna_visina: 167,
        telesna_teza: 58.00,
        telesna_temperatura: 36.50,
        sistolicni_tlak: 132,
        diastolicni_tlak: 86,
        nasicenost_kisik: 98

    }
};

var oseba3 = {
    num: 3,
    ime: 'Rajko',
    priimek: 'Mitrovic',
    datum_rojstva: '1940-11-07',
    vitalni_znaki: {

        datum_ura: '2019-05-05T12:44Z',
        telesna_visina: 167,
        telesna_teza: 58.00,
        telesna_temperatura: 36.50,
        sistolicni_tlak: 183,
        diastolicni_tlak: 115,
        nasicenost_kisik: 98

    }
};


var staticData = [oseba1, oseba2, oseba3];

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
    return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {

    let person = staticData[stPacienta - 1];

    insertOsebaIntoEhr(person);

    return person.ehr;

}

function insertOsebaIntoEhr(oseba) {

    createEHR(function (data) {

        var ehrID = data.ehrId;

        console.log(data);

        addBasicInfoToEHR(oseba, ehrID, function (data) {
            console.log(data);
            addVitalSigns(oseba, ehrID, function (data) {
                console.log(data);

                oseba.ehr = ehrID;

            }, function (err) {
                console.log(err);
            })

        }, function (err) {
            console.log(err);
        })

    }, function (error) {
        console.log(error);
    })

}

function createEHR(callback, error) {
    $.ajax({
        url: baseUrl + '/ehr',
        type: 'POST',
        headers: {
            'Authorization': getAuthorization()
        },
        success: callback,
        error: error,
        async: false
    })
}

function addBasicInfoToEHR(person, ehrID, callback, error) {

    var info = {
        firstNames: person.ime,
        lastNames: person.priimek,
        dateOfBirth: person.datum_rojstva,
        additionalInfo: {
            "ehrId": ehrID
        }
    };

    console.log(JSON.stringify(info));

    $.ajax({
        url: baseUrl + '/demographics/party',
        type: 'POST',
        headers: {
            'Authorization': getAuthorization()
        },
        contentType: 'application/json',
        data: JSON.stringify(info),
        success: callback,
        error: error,
        async: false
    });

}

function addVitalSigns(person, ehrID, callback, error) {
    var parametriZahteve = {
        ehrId: ehrID,
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: 'Boban'
    };

    var podatki = {
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": person.vitalni_znaki.datum_ura,
        "vital_signs/height_length/any_event/body_height_length": person.vitalni_znaki.telesna_visina,
        "vital_signs/body_weight/any_event/body_weight": person.vitalni_znaki.telesna_teza,
        "vital_signs/body_temperature/any_event/temperature|magnitude": person.vitalni_znaki.telesna_temperatura,
        "vital_signs/body_temperature/any_event/temperature|unit": "°C",
        "vital_signs/blood_pressure/any_event/systolic": person.vitalni_znaki.sistolicni_tlak,
        "vital_signs/blood_pressure/any_event/diastolic": person.vitalni_znaki.diastolicni_tlak,
        "vital_signs/indirect_oximetry:0/spo2|numerator": person.vitalni_znaki.nasicenost_kisik
    };

    $.ajax({
        url: baseUrl + "/composition?" + $.param(parametriZahteve),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(podatki),
        headers: {
            "Authorization": getAuthorization()
        },
        success: callback,
        error: error,
        async: false
    })

}

function getBloodPressure(ehrId, callback) {
    $.ajax({
        type: 'GET',
        url: baseUrl + '/view/' + ehrId + '/' + 'blood_pressure',
        headers: {
            "Authorization": getAuthorization()
        },
        success: callback,
        error: callback
    })
}

function getPersonInfo(ehrId, callback, error) {

    $.ajax({
        type: 'GET',
        url: baseUrl + '/demographics/ehr/' + ehrId + '/party',
        headers: {
            "Authorization": getAuthorization()
        },
        success: callback,
        error: error
    })

}

function calculatePersonAge(birthday) {
    var firstDate = Date.parse(birthday);
    var diff = firstDate - Date.now();
    var newDateObj = new Date(diff);
    return Math.abs(newDateObj.getFullYear() - 1970);
}

function createSistolicChart(sistolic) {

    var ctx = document.getElementById('myChartSi').getContext('2d');
    myChartSis = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Vaš krvni tlak', 'Normalen krvni tlak'],
            datasets: [{
                label: 'Sistolični krvni tlak',
                data: [sistolic, referenceData.sistolic],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(75, 192, 192, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

}

function createDistolicChart(distolic) {
    var ctx = document.getElementById('myChartDi').getContext('2d');
    myChartDis = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Vaš krvni tlak', 'Normalen krvni tlak'],
            datasets: [{
                label: 'Distolični krvni tlak',
                data: [distolic, referenceData.diastolic],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(75, 192, 192, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function insertDataIntoTable(data) {
    $('#datum_data').html(data.time);
    $('#diastolic_data').html(data.diastolic + " " + data.unit);
    $('#systolic_data').html(data.systolic + " " + data.unit);
}

function masterDetailSwitch(id) {

    if (id === currentActive) {
        return;
    }
    else {
        $(".vzroki-item[data-pannel='" + currentActive + "']").removeClass('vzroki-item-active');
        $(".vzroki-content-item[data-pannel='" + currentActive + "']").hide();

        $(".vzroki-item[data-pannel='" + id + "']").addClass('vzroki-item-active');
        $(".vzroki-content-item[data-pannel='" + id + "']").show();

        currentActive = id;
    }
}

function prepareData(ehrID) {

    $('#errorMessage').hide();

    if (ehrID === 'none') {
        $('.content-container').hide();
    }
    else {
        getPersonInfo(ehrID, function (personInfo) {
            $('.page-header h1').html(personInfo.party.firstNames + " " + personInfo.party.lastNames);
            getBloodPressure(ehrID, function (bloodPressure) {

                var results = determineTypeOfHypertensia(bloodPressure[0]);

                insertDataIntoTable(bloodPressure[0]);
                $('#diagnose').html(results.message);

                if (results.sick) {
                    $('.vzroki').show();
                    $('#vreme').hide();
                }
                else {
                    $('.vzroki').hide();
                    $('#vreme').show();
                }

                if (first) {
                    createSistolicChart(bloodPressure[0].systolic);
                    createDistolicChart(bloodPressure[0].diastolic);
                    first = false;
                }
                else {
                    myChartDis.destroy();
                    myChartSis.destroy();
                    createSistolicChart(bloodPressure[0].systolic);
                    createDistolicChart(bloodPressure[0].diastolic);
                }

                $('.content-container').show();

            }, function (error) {
                $('#errorMessage').show();
            })
        }, function (err) {
            $('#errorMessage').show();
        })
    }
}

function getWeatherForCity(city, callback) {
    $.ajax({
        type: 'get',
        url: 'https://api.openweathermap.org/data/2.5/weather?q=' + city + '&APPID=2e0a52bce826899514afac921e9a6891&units=metric',
        success: callback,
        error: callback
    });
}

function prepareWeather() {
    var city = $('#city').val();
    city = city.trim();
    getWeatherForCity(city, function (weather) {

        console.log(weather);

        $('#weather-temperature').html(Math.floor(weather.main.temp) + "°C");

        var img_url = 'https://openweathermap.org/img/w/' + weather.weather[0].icon + '.png';

        $('#weather-icon img').attr('src', img_url);
        $('#weather-icon').show();
    })
}

function determineTypeOfHypertensia(bloodPressure) {
    var dis = bloodPressure.diastolic;
    var sis = bloodPressure.systolic;
    if (sis >= 180 || dis >= 110) {
        return {
            message: "<b>Hipertenzija 3. stopnje,</b> imate povišan krvni tlak, poglejte si morebitne vzroke in rešitve",
            sick: true
        }
    }
    if (sis >= 160 || dis >= 100) {
        return {
            message: "<b>Hipertenzija 2. stopnje,</b> imate povišan krvni tlak, poglejte si morebitne vzroke in rešitve",
            sick: true
        }
    }
    if (sis >= 140 || dis >= 90) {
        return {
            message: "<b>Hipertenzija 1. stopnje,</b> imate povišan krvni tlak, poglejte si morebitne vzroke in rešitve",
            sick: true
        }
    }
    if (sis >= 130 || dis >= 85) {
        return {
            message: "<b>Visoko normalen krvni tlak</b>, imate rahlo povišan krvni tlak. Morda na vas upliva vremenska sprememba (nižja temperatura povzroča višji krvni tlak). Preverite vreme odspodaj",
            sick: false
        }
    }
    if (sis >= 120 || dis >= 80) {
        return {
            message: "<b>Normalen krvni tlak</b>, če prihaja do majhnega odstopanja od idealne vrednosti krvnega tlaka (120/80), je lahko za to vzrok vreme",
            sick: false
        }
    }
    else {
        return {
            message: "<b>Optimalen krvni tlak</b> na nižji krvni tlak lahko upliva sprememba vremena(višja temperatura povzroča nižji krvni tlak). Prevereite vreme odspodaj",
            sick: false
        }
    }
}


$(window).load(function () {
    $('#izberiPacienta').change(function (event) {
        prepareData(event.target.value)
    });

    $('#naloziButton').click(function () {
        prepareData($('#ehrIdInput').val());
    });

    $('.vzroki-content-item:first-child').show();
    $('.vzroki-item').click(function () {
        var id = $(this).attr('data-pannel');
        masterDetailSwitch(id);
    });

    $('#weather-icon').hide();
    $('#weatherSubmit').click(prepareWeather);

    $('#generirajPodatke').click(function(event){
        event.preventDefault();
        
        for(var i = 2; i <= staticData.length + 1; i++){
            $('.pacient:nth-child(' + i + ')').val(generirajPodatke(i - 1)).html(staticData[i - 2].ime + " " + staticData[i - 2].priimek);
        }

        $('#izberiPacienta').val('none');
        prepareData('none');
    })

});